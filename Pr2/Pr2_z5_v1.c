/*
�������� ���������, ������� ������� �� ����� 10 �������,
��������������� ��������� ������� �� ��������� ���� � ����,
������ ����� ������ ���� ��� � ������, ��� � � ������� ���������.
����� ������ 8 ��������.
���������:
������ ���������������� ������: Nh1ku83k.
*/
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

int main ()
{
	char a;
	int i, j;
	
	srand (time(NULL));

	for (j=1; j<=10; j++)
	{
		for (i=0; i<8; i++)
		{
			switch (rand ()%3)
			{
				case 0:
					{
						a=rand () %10 + '0';
						break;
					}
				case 1:
					{
						a=rand () %26 + 'a';
						break;
					}
				case 2:
					{
						a=rand () %26 + 'A';
						break;
					}
			}
			printf ("%c",a);
		}
		printf ("\n");
	}

	return 0;
}