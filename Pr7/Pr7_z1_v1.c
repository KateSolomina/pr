/*
�������� ���������, ������� ����������� �������� ��� ����� ��
����� �� � ������� ������� ������������� �������� ���� �����.
�������� ����� ����� �������� � ��������� �����.
���������:
(a) ����������� ���� � ��������� ������� � �������� �������� ������ �
�������������� ������� ��� ���� ����;
(b) ����������� ���� � ��������� �������;
(c) ����������� ������������� ���� � �������� �����������;
(d) ������� ����������� �� ������ � ������, � ����� ���������� ������
������ � �������� ������;
(e) ���� ����� ��������� � ���������� � ������, ����������� ������� 
������������� ������� �����;
(f) ������������� ������ ��������� ������ ���, ��� ������ ����� �������
��� ����������� ������ �����������.
���������:
��������� ������ �������� �� ��������� �������:
(a) chomp - �������� ������� ����� ������.
(b) makeTree - �������� ������ �������� ����.
(c) searchTree - ����� � ������ ������� ���������� ��������.
(d) printTree - ������ ������ �������� ���� � �� ����������.
(e) main - ������� ������� ���������.
*/
#include <stdio.h>
#include <wctype.h>

#define N 256

struct tree
{
	char buf[N];
	int i;
	struct tree *left;
	struct tree *right;
};

void chomp(char *buf)
{
	if (buf[strlen(buf)-1]=='\n')
		buf[strlen(buf)-1]='\0';
}

int makeTree(struct tree *root, char *f)
{
	if (root==NULL)
	{
		struct tree *temp=(struct tree*)malloc(sizeof(struct tree));
		temp->i=1;
		strcpy(temp->buf, f);
		temp->left=NULL;
		temp->right=NULL;
		return temp;
	}
	else if(strcmp(root->buf,f)>0)
	{
		root->left=makeTree(root->left, f);
		return root;
	}
	else if(strcmp(root->buf,f)<0)
	{
		root->right=makeTree(root->right, f);
		return root;
	}
	else
	{
		root->i++;
		return root;
	}
}
void printTree(struct tree *root)
{
	if (root!=NULL)
	{
		if (root->left)
			printTree(root->left);
		printf("%s - %d\n", root->buf, root->i);
		if (root->right)
			printTree(root->right);
	}
}
void searchTree(struct tree *root, char *f)
{
	if (root!=NULL)
	{
		if(strcmp(root->buf,f)>0)
		{
			searchTree(root->left,f);
		}
		else if(strcmp(root->buf,f)<0)
			searchTree(root->right,f);
		else if(strcmp(root->buf,f)==0)
			root->i++;
	}
}
int main(int argc, char* argv[])
{
	char buf[N];
	char Arr[N];
	signed char c;
	int i=0;
	int word=0;
	int j;
	struct tree *root=NULL;
	FILE *fp;

	if (argc!=3)
		printf ("Error! Enter Parameters!\n");

	//file with keys opens, tree makes, file with keys closes
	fp=fopen(argv[1], "rt");
	if(fp == NULL)
	{
		printf("Error! First file not found\n");
		return 1;
	}

	while (!feof (fp))
    {
        fscanf (fp, "%s", buf);
        chomp (buf);
		root = makeTree(root, buf);
    }
	fp=fclose(argv[1]);

	//file opens
	fp=fopen(argv[2], "rt");
	if(fp == NULL)
	{
		printf("Error! Second file not found\n");
		return 1;
	}
	while ((c = fgetc(fp)) != EOF) 
	{
		if (iswalpha(c))
		{
			Arr[i]=c;
			i++;
			word=1;
		}
		else if ((iswalpha(c)==0) && (word==1))
		{
			searchTree(root, Arr);
			for (j=0; j<=i; j++)
				Arr[j]=0;
			i=0;
			word=0;
		}
	}
	fp=fclose(argv[2]);

	//print
	printTree(root);

	return 0;
}