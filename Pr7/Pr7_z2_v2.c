/*
�������� ��������� , ������� ������ ������� ������������� 
�������� ��� ������������� �����, ��� �������� �������� �
��������� ������. ��������� ������ �������� �� ����� �������
�������������, ��������������� �� �������� �������.
���������:
� ��������� ���������� ���������� ����������� ��� SYM, �
������� ����� ������� ��� ������� � ������� �������������
(������������ ����� �� 0 �� 1). ����� ������� �����,
������ �������� SYM ������ ���� ������������ �� �������.
*/
#include <stdio.h>
#define N 256

typedef struct
{
	char ch;
	int count;
	float freq;
}SYM;

int main(int argc, char* argv[])
{
	SYM Arr[N]={NULL};
	char c;
	int i=0,j=0;
	int count=0;
	FILE *fp;
	SYM temp;

	if (argc!=2)
		printf("Enter parameters!\n");

	fp=fopen(argv[1],"rt");
	if(fp == NULL)
	{
		printf("Error! File not found!\n");
		return 1;
	}

	while ((c = fgetc(fp)) != EOF)
	{
		Arr[(int)c].ch = c;
		Arr[(int)c].count++;
		j++;
	}

	fclose(fp);

	for (i=0; i<N; i++)
		if (Arr[i].count != 0)
			count++;
	for (i=0; i<N; i++)
		if (Arr[i].count != 0)
			Arr[i].freq = (float)Arr[i].count/j;
	
	for (i=1; i<N; ++i)
	{
		j=i;
		temp = Arr[i];
		while (j>0 && temp.count < Arr[j - 1].count)
		{
			Arr[j] = Arr[j - 1];
			j--;
		}
		Arr[j] = temp;
	}
	for (i=0; i<N; i++)
		if (Arr[i].count != 0)
			printf ("%c - %lf\n", Arr[i].ch, Arr[i].freq);
	return 0;
}