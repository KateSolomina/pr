/*
�������� ���������, ������� ��������� ������������ ������ ��������� �����
� ����������, � ����� ��������� �� � ������� ����������� ����� ������.
���������:
������ �������� �� ��������� ������ ������ � ������������ � ���������
���������� ������. ������������ ���������� �������� � ���������� ������� 
���������� �� char. ����� ��������� ����� ��������� ��������� ���������
� ������� � ������� ������ � ������������ � ���������������� �����������.
*/
#include <stdio.h>
#include <math.h>
#include <windows.h>
#include <stdlib.h>

int main ()
{
	char Arr[256][80];
	char *Adr[256];
	char *temp;
	int i=0, j, k;

	printf ("Enter string\n");

	while (1)
	{
		fflush(stdin);
		gets (Arr[i]);
		if (Arr[i][0]=='\0')
			break;
		else
		{
			Adr[i]=&Arr[i][0];
			i++;
		}
	}
	
	for (k=0; k<i; k++)
	{
		temp=Adr[k];
		for (j=0; j<i-1; j++)
		{
			if (strlen(Adr[j]) > strlen(Adr[j+1]))
			{
				temp=Adr[j];
				Adr[j]=Adr[j+1];
				Adr[j+1]=temp;
			}
		}
	}

	for (k=0; k<i; k++)
		printf ("%s\n",Adr[k]);
	return 0;
}