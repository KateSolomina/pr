/*
�������� ���������, ����������� ������ (��. ������ 1), �� ������������
������, ���������� �� ���������� �����. ��������� ������ ��������� ����� 
������������ � ����
*/

#include <stdio.h>

int main()
{
	char Arr[256][256];
	char *Adr[256];
	char *temp;
	int i=0, j, k, str_len;

	FILE *fp;

	fp = fopen("Pr4_z5.txt", "r+");
	if (fp == NULL)
	{
		puts ("File error!");
		return 1;
	}

	while (fgets (Arr[i],256, fp))
	{
		if (Arr[i][0]=='\n')
			break;
		else
		{
			Adr[i]=&Arr[i][0];
			i++;
		}
	}
	
	for (k=0; k<i; k++)
	{
		temp=Adr[k];
		for (j=0; j<i-1; j++)
		{
			if (strlen(Adr[j]) > strlen(Adr[j+1]))
			{
				temp=Adr[j];
				Adr[j]=Adr[j+1];
				Adr[j+1]=temp;
			}
		}
	}

	fprintf (fp,"\n\n\n%s\n\n", "Sort string:");
	for (k=0; k<i; k++)
	{
		fprintf (fp,"%s",Adr[k]);
		printf ("%s",Adr[k]);
	}

	fclose (fp);

	return 0;
}