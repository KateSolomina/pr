/*
�������� ���������, ������� ����������� ���������� ������������� � �����,
� ����� ����������� ������ ��� ������������ � ��� �������. ��������� ������
���������� ������ �������� � ������ ������� ������������.
���������:
����� ������� ������ ����� ��� �������� ���� � ��� ���������: young � old,
������� �� ���� �����, ��������� � ������� ��������.
*/

#include <stdio.h>

int main ()
{
	char ArrName[10][80];
	int ArrAge[10];
	int *young, *old;
	char *young_name, *old_name;
	int i, n;

	printf ("Enter digite\n");
	scanf ("%d", &n);

	i=0;
	printf ("Enter Name, Age (format Name Age)\n");

	while (i < n)
	{
		fflush(stdin);
		scanf ("%s %d", &ArrName[i], &ArrAge[i]);
		i++;
	}

	young=ArrAge;
	old=ArrAge;

	for (i=0; i<n; i++)
	{
		if (ArrAge[i]<=*young)
		{
			young=&ArrAge[i];
			young_name=&ArrName[i];
		}
		if (ArrAge[i]>=*old)
		{
			old=&ArrAge[i];
			old_name=&ArrName[i];
		}
	}

	printf ("\n\nYoung is %s %d.\nOld is %s %d.\n", young_name, *young, old_name, *old);
	return 0;
}