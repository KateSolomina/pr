/*
�������� ���������, ������� � ������� ������� ���������� ������� �����
������ � �������� �������
���������:
������ �� ������� ��������� ������ ���������� �� char, � ������� ���������
������ ������ �������� ������� ����� (������������ - ������ ������� � ����������
��������). ����� �� ���������� ����� ����� ������, ��������� ���� ������
����������.
*/
#include <stdio.h>
#include <math.h>
#include <windows.h>
#include <stdlib.h>

int main ()
{
	char Arr[80];
	char *pArr[80];
	char temp[80];
	int i = 0, j=0, str_len, count=0;
	short flag = 0;

	printf ("Enter string\n");
	gets (Arr);

	str_len = strlen (Arr);

	while (Arr[i])
	{
		if (Arr[i] != ' ' && flag == 0)
		{
			flag = 1;
			pArr[count++] = &Arr[i];
		}
		else if (Arr[i] == ' ' && flag == 1)
		{
			Arr[i] = '\0';
			flag = 0;
		}
		i++;
	}

	for (i = count-1; i >= 0; i--)
		printf ("%s ", pArr[i]);
	printf ("\n");

	return 0;
}