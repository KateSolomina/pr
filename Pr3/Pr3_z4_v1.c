/*
�������� ���������, ������� ������� ����� ����� �� ��������� ������.
���������:
��������� ������������� ����������� ������������������ ���� � ������ ��� �����
� ������������ �� ��� ������ �����. � ��������� ������������� �����������
�� ������������ ����� ��������, �� ���� ���� ������������ ������ �����
������� ������������������ ����, �� ����� ������� �� ��������� �����.
*/
#include <stdio.h>
#include <math.h>
#include <windows.h>
#include <ctype.h>
#define N 4 //max digite

int main ()
{
	int max_dig=0,str_len;
	char Arr[80],temp[2]={0};
	int i;
	int dig=0,count=1;

	printf ("Enter string of numbers\n");
	gets (Arr);

	printf ("%s\n", Arr);
	str_len=strlen(Arr);
	for (i=0;i<=str_len;i++)
	{
		if (isdigit(Arr[i]) && count<N)
		{
			temp[0]=Arr[i];
			dig=dig*10+atoi(temp);
			count++;
		}
		else
		{
			max_dig+=dig;
			dig=0;
			count=1;
		}
	}

	printf ("max=%d\n", max_dig); 
	return 0;
}