/*
�������� ���������� ����������� �������, ����������� n-��
������� ���� ���������, �� ��� ��������������� �������� 
��������
���������:
����� ������� ��� �������: ���� ���������� ��������������� �� main
� �������� ������, ���������������, ������� � �������� �����������.
*/
#include <stdio.h>
#include <windows.h>

int step(int a, int b, int i)
{
	if (i==1 || i==2)
		return 1;
	else
		return a+b;
}

int fib(int i)
{
	static int a, b;
	int sum;

	sum=step(a,b,i);
	a=b;
	b=sum;
	Sleep(500);
	return sum;
}

int main ()
{
	int i;
	int sum;

	for (i=1; i<=40; i++)
	{
		sum=fib(i);
		printf("%d	%d\n", i, sum);
	}
	return 0;
}