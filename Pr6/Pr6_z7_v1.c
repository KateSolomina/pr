/*
�������� ���������, ������� ������� ����� �� ���������
���������:
(�) �������� �������� � ���� ���������� ����������� �������;
(b) ��������� ������� � ������;
(c) ��������� ���������� (�� �������), ��� ��������� ������ 
������� ������;
(d) ���� ������ ��������, ��������� ������������ � ������ ����� � 
��� �����������;
(e) ��������� ������ � ������ �� ��������� ��� ����������� ���
������� �������.
���������:
�������� '#' ���������� ����� ���������
�������� ������ - ��������� �������
�������� 'X' - �������������� ��������

############################
#			#   #          #
##########  #   #          #
#			#	#######  ###
# ######    #X			   #
#	   #	#	#######	  ##
#####  #	#	#		  ##
	   #		#	  ######
############################
*/
#include <stdio.h>
#include <windows.h>
#define N 9
#define M 28

char step (char(*Arr)[M], int x, int y, char parent)
{
	if ((parent=='r') || (parent=='0'))
	{
		Arr[x][y]=' ';
		Arr[x][y+1]='X';
		y=y+1;
		return y;
	}
	else if (parent=='t')
	{
		Arr[x][y]=' ';
		Arr[x-1][y]='X';
		x=x-1;
		return x;
	}
	else if (parent=='l')
	{
		Arr[x][y]=' ';
		Arr[x][y-1]='X';
		y=y-1;
		return y;
	}
	else if (parent=='b')
	{
		Arr[x][y]=' ';
		Arr[x+1][y]='X';
		x=x+1;
		return x;
	}
}
void printArr(char (*Arr)[M])
{
	int i,j;
	for (i=0; i<N; i++)
	{
		for (j=0; j<M; j++)
			printf("%c",Arr[i][j]);
		printf ("\n");
	}
}

char findParent(char (*Arr)[M], int x, int y, char parent)
{
	char xr, xl, yt, yb;

	yt=Arr[x-1][y];
	yb=Arr[x+1][y];
	xr=Arr[x][y+1];
	xl=Arr[x][y-1];

	if (parent !='0')
	{
		if (parent=='r')
		{
			if (yb==' ')
				return 'b';
			else if (xr==' ')
				return 'r';
			else if (yt==' ')
				return 't';
			else 
				return 'l';
		}
		if (parent=='t')
		{
			if (xr==' ')
				return 'r';
			else if (yt==' ')
				return 't';
			else if (xl==' ')
				return 'l';
			else 
				return 'b';
		}
		if (parent=='l')
		{
			if (yt==' ')
				return 't';
			else if (xl==' ')
				return 'l';
			else if (yb==' ')
				return 'b';
			else 
				return 'r';
		}
		if (parent=='b')
		{
			if (xl==' ')
				return 'l';
			else if (yb==' ')
				return 'b';
			else if (xr==' ')
				return 'r';
			else 
				return 't';
		}
		else
			return 'r';
	}
	else if ((xr=='#') || (xl=='#') || (yt=='#') || (yb=='#'))
	{
		if ((xr=='#') && (yt==' '))
			return 't';
		else if ((yt=='#') && (xl==' '))
			return 'l';
		else if ((xl=='#') && (yb==' '))
			return 'b';
		else if ((yb=='#') && (xr==' '))
			return 'r';
		else 
			return 'e';
	}
	else 
		return '0';
}

int lab(char (*Arr)[M], int x, int y)
{
	int i, j;
	char parent;
	parent ='0';
	
	while ((x!=7) || (y!=0))
	{
		parent=findParent(Arr, x, y, parent);
		if ((parent=='r') || (parent=='l') || (parent=='0'))
			y=step (Arr, x, y, parent);
		if ((parent=='t') || (parent=='b'))
			x=step (Arr, x, y, parent);
		if (parent=='e')
		{
			printf ("error\n");
			return 1;
		}
		Sleep (500);
		printArr(Arr);
	}
	return 0;
}

int main ()
{
	char Arr[N][M];
	char *pArr[N];
	int i, j;
	int x, y;
	char xl, xr, yt, yb;
	int count;

	FILE *fp;

	fp=fopen("lab.txt", "rt");

	if(fp == NULL)
	{
		printf("Error");
		return 1;
	}

	for (i = 0; i < N; i++)
	{
		for (j = 0; j < M; j++)
		{
				Arr[i][j] = fgetc(fp);

			if (Arr[i][j] == '\r')
				Arr[i][j] = fgetc(fp);
			if (Arr[i][j] == '\n')
				Arr[i][j] = fgetc(fp);
			if (Arr[i][j] == 'X')
			{
				x=i;
				y=j;
			}
		}

		pArr[i] = &Arr[i][0];
	}

	fclose(fp);

	printArr(Arr);
	lab(Arr, x,y);

	return 0;
}