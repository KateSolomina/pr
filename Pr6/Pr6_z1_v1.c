/*
�������� ���������, ������� ��������� � ��������� ����������
������� ����������� ����������� � ������� ��� �� ������� (��.
�������)
���������:
             *
            ***
			 *
		  *  *  *
		 *********
		  *  *  *
			 *
			***
			 *
    *		 *		  *
   ***		***		 ***
	*		 *		  *
 *  *  *  *  *  *  *  *  *
***************************
 *  *  *  *  *  *  *  *  *
	*		 *		  *
   ***      ***		 ***
	*	     *		  *
             *
            ***
			 *
		  *  *  *
		 *********
		  *  *  *
			 *
			***
			 *
��������� ������ �������� �����������, ����� ����� �������� �������
������. �������, ��� ��������� �������� ���������� �������� � ���������
���������� �����������, ������������ ������ �����������.
*/
#include <stdio.h>

#define N 243

void clearArr(char *pArr[], int x, int y, int n)
{
	int i, j;
	int k, l;

	if (n!=1)
	{
		for (i=0; i<n/2; i++)
			for (j=0; j<n/2; j++)
				if (j < n/2 - i)
					*(pArr[x+i] + y+j)=' ';
		for (i=n/2+1; i<n; i++)
			for (j=0; j<n/2; j++)
				if (j < i - n/2)
					*(pArr[x+i] + y+j)=' ';
		for (i=0; i<n/2; i++)
			for (j=n/2; j<n; j++)
				if (j > n/2 + i)
					*(pArr[x+i] + y+j)=' ';
		for (i=n/2; i<n; i++)
			for (j=n/2; j<n; j++)
				if (j > n + n/2 - 1 - i)
					*(pArr[x+i] + y+j)=' ';
		if (n/3>0)
		{
			for (k=0; k<3; k++)
				for (l=0; l<3; l++)
					clearArr(pArr, y+(n / 3)*k, x+(n / 3)*l, n/3);
		}
	}
}

int main ()
{ 
	int i, j;
	int count;
	char Arr[N][N];
	char *pArr[N];

	for (i=0; i<N; i++)
	{
		for (j=0; j<N; j++)
			Arr[i][j]='*';
		pArr[i]=&Arr[i][0];
	}	

	clearArr(pArr, 0, 0, N);

	for (i=0; i<N; i++)
	{
		for (j=0; j<N; j++)
			printf ("%c", Arr[i][j]);
		printf ("\n");
	}	

	return 0;
}