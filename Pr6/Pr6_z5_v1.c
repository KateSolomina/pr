/*
�������� ���������, ������� �������� ����� ���������� N-���
����� ���� ���������. ������������� ����� ������� ��������
��� N � ��������� �� 1 �� 40 (��� � ������ ��������� �� �������)
�� ����� � � ����.
���������:
��������� ���� �� ���������� ����� ������� � ����������� �������
� ��������� ������ ����������� ������� �� ����� ���� N
*/
#include <stdio.h>
#include <time.h>

int fibN( int j, int k, int n)
{
	if (n==1)
		return k;
	else
		return fibN(j+k, j, n-1);
}
int fib(int i)
{
	return fibN(0, 1, i);
}

int main()
{
	int i;
	int start, end;
	FILE *fp;
	fp = fopen ("fib.xls","wt");
	if (fp==NULL)
	{
		puts ("File error!");
		return 1;
	}

	printf ("i		fib(i)		time\n");
	fprintf (fp, "i	 fib(i)	time\n");
	for (i=1; i<=40; i++)
	{
		start = clock();
		fib (i);
		end = clock();
		printf ("%d		%d		%d\n", i, fib(i), start-end);
		fprintf (fp, "%d	%d	%d\n", i, fib(i), start-end);
	}

	fclose (fp);
	return 0;
}