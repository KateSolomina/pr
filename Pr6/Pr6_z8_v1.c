/*
�������� ���������, ������� ��������� ������������� ���������
��������������� ���������, ��������� � ���� ��������� ���������
������. ������������� ��������� 4-� �������� ��������. ������� 
���������� ������������ �������� ��������
���������:
� ������ ��������� ����� ����������� �������: 0-9,+,-,*,/,(,).
��������� ����� ���� ��������, �� ���� �������� �� ������ ����� 
3, 8, � ����� ���� ��������, ��������, ((6+8)*3) ��� (((7-1)/(4+2))-9). 
��������������, ��� ������ � ��������� ������ ���������, �� ���� 
���������� �������� ����� ���������� �������� � ��� �� ���������� ��������.
��������� ������ �������� �� ��������� �������:
(a) int main (int argc, char* argv[]) - ������� �������, � �������
�������������� ����� ����������� ������� eval ��� ���������� ���������
(b) int eval (char *buf) - �������, ����������� ������, ������������ � buf
(c) char partition (char *buf, char *expr1, char *expr2) - �������, �������
��������� ������, ������������ � buf �� ��� �����: ������ � ������ ���������, 
���� �������� � ������ �� ������ ���������
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>

int partition (char *buf, char *expr1, char *expr2)
{
	int i,j;
	int start=0, end=0;
	int count=0;
	char symb;
	int str_len;	
	int flag=0;

	str_len=strlen(buf);
	if (buf[str_len-1]=='\n')
		str_len-=1;
	//delete start '(' and end ')' if it necessary
	if ((buf[0]=='(') && (buf[str_len-1]==')'))
	{
		for (i=0; i<str_len; i++)
		{
			if (buf[i]=='(')
				count++;
			else if (buf[i]==')')
			{
				count--;
				if ((count==0) && (i!=str_len-1))
					flag=1;
			}
		}
		if (flag==0)
		{
			for (i=0; i<str_len-1; i++)
				buf[i]=buf[i+1];
			buf[str_len-2]='\0';
			str_len-=2;
		}
	}
	//find expr1 and expr2	
	if (buf[0]=='(')
	{
		if (buf[str_len-1]==')')
			count=-1;
		for (i=0; i<str_len; i++)
		{
			if (buf[i]=='(')
				count++;
			if (buf[i]==')')
			{
				count--;
				if ((count==0)||(count==-1))
				{
					end=i;
					break;
				}
			}
		}
		for (i=0; i<=end; i++)
			expr1[i]=buf[i];
		j=0;
		symb=buf[end+1];
		for (i=end+2; i<str_len; i++, j++)
			expr2[j]=buf[i];
	}
	else
	{
		for (i=0; i<str_len; i++)
			if (isdigit(buf[i])==0)
			{
				end=i;
				break;
			}
		for (i=0; i<end; i++)
			expr1[i]=buf[i];
		j=0;
		symb=buf[end];
		for (i=end+1; i<str_len; i++, j++)
			expr2[j]=buf[i];
	}

	//find rez
	if (symb=='+')
		return eval(expr1)+eval(expr2);
	if (symb=='-')
		return eval(expr1)-eval(expr2);
	if (symb=='*')
		return eval(expr1)*eval(expr2);
	if (symb=='/')
		return eval(expr1)/eval(expr2);
}
int eval (char *buf)
{
	char expr1[256]={' '};
	char expr2[256]={' '};
	int flag=0;
	int rez;
	char *Arr;

	Arr=buf;
	while (*Arr != 0) 
	{ 
        if (*Arr == '+' || *Arr == '-' || *Arr == '*' || *Arr == '/')
            flag = 1;
        Arr++;
    }
    if (flag == 0)
        return atoi (buf);
    else 
		rez=partition(buf, expr1, expr2);

	return rez;
}

int main (int argc, char* argv[])
{
	int i;
	char buf[256];

	for(i = 0; i < argc; i++)
		puts(argv[i]);
	if (argc>1)
	{
		strcpy (buf, argv [1]);
		printf ("%s=%d\n",argv[1], eval(buf));
	}
	else 
		printf ("Enter parameters!\n");
 
    return 0;
}