/*
�������� ���������, ������� ��������� ������ ������������ � 
����������� ���������.
���������:
��������� ��������� ��������� ������������������ ��������:
(a) ��������� �� ��������� ������ �������� ������� ������ �;
(b) ������� ������ ������������� ������� N=2^M;
(c) �������� ������ ��� ������������ ������;
(d) ��������� ������� ��������� ������ �������;
(e) ������� ����� ������������ � ����������� ��������;
(f) ���������� ����� ���������� ������������ ������������
� ����������� ��������;
(g) ����������� ������������ ������.
*/
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

int SumT(int *Arr, int n)
{
	int i;
	int sum=0;
	for (i=0; i<n; i++)
		sum=sum+Arr[i];
	return sum;
}

int SumR(int *Arr, int sum, int n)
{	
	if (n)
	{
	sum=sum+Arr[n-1];
	SumR(Arr, sum, n-1);
	}
	return sum;
	
}

int main ()
{
	int n, i;
	int *Arr;
	int SumTr, SumRec;
	int start, end, TimeTr, TimeRec;

	srand (time(NULL));

	printf ("Enter digite (1...28)\n");
	scanf ("%d", &n);

	n=(int)pow(2.0,(double)n);
	printf ("n=%d\n", n);


	Arr=(int*)malloc(sizeof(int)*n);
	
	for (i=0; i<n; i++)
	{
		*(Arr+i)=rand () %100;
	}

/*	for (i=0; i<n; i++)
	{
		printf ("%d ", Arr[i]);
	}
	printf ("\n");*/

	start=clock();
	SumTr=SumT(Arr, n);
	end=clock();
	TimeTr=end-start;

	start=clock();
	SumRec=SumR(Arr, 0, 0, n);
	end=clock();
	TimeRec=end-start;

	if (TimeTr>TimeRec)
		printf ("TimeTr > TimeRec\nTimeTr = %d\nTimeRec = %d\n", TimeTr, TimeRec);
	else if (TimeTr<TimeRec)
		printf ("TimeTr < TimeRec\nTimeTr = %d\nTimeRec = %d\n", TimeTr, TimeRec);
	else
		printf ("TimeTr = TimeRec\nTimeTr = %d\nTimeRec = %d\n", TimeTr, TimeRec);

	return 0;
}