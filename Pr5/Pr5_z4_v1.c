/*
�������� ���������, ������� ������ ��������� ��������� ���� � ������������ 
�������� ����� � ������ ������.
���������:
��������� ��������� ������������ ��������� ���� � ������ ��� ���������. ���
������ ������ ���������� �������, ������������� � ������ ������ 1.
*/
#include <stdio.h>

#define n 256
#define m 256

void printWord(char *temp)
{
	while (*temp != '\0')
	{
		putchar (*temp);
		if(*(temp + 1) != '\0')
			temp++;
		else
			break;
	}
	putchar(' ');
}

char getWords(char *Arr, char **pArr, int *j)
{
	int i,flag;
	int str_len;
	flag=0;

	str_len=strlen(Arr);

	for (i=0; i<str_len; i++)
	{
		if (flag==0 && (Arr[i]!=' ' || Arr[i]!='\n'))
		{
			flag=1;
			pArr[*j]=&Arr[i];
			(*j)++;
		}
		else if (flag==1 && (Arr[i]==' ' || Arr[i]=='\n'))
		{ 
			flag=0;
			Arr[i]='\0';
		}
	}
	return pArr;
}

int main()
{
	char Arr[n][m];
	char *pArr[n];
	char temp;
	int i=0, j, count,k;
	FILE *fp;

	fp = fopen("Pr5_z4.txt", "rt");
	if (fp == NULL)
	{
		puts ("File error!");
		return 1;
	}

	while (fgets (Arr[i],m, fp))
	{
		if (Arr[i][0]=='\n')
			break;
		else
		{
			k=i;
			printf ("%s", Arr[i]);
			getWords(Arr[i], pArr, &i);
			j=i;
//===============
			for (i=k; i<j; i++)
			{
				count=rand()%(j-i);
				printWord(pArr[k+count]);
				temp=pArr[k+count];
				pArr[k+count]=pArr[k+j-i-1];
				pArr[k+j-i-1]=temp;
			}
//===============
			printf ("\n");
		}
	}
	
	fclose (fp);

	return 0;
}