/*
�������� ��������� "�����������", ��������� �� ����� �����������, ������������ �� 
����������� ������������� ��������� "*". ����������� ����������� � ��������� ���������� 
�������, � ����� ��� ����� � ����������� ���������� � ��������� ��� �����.
���������:
������� ������ ��������� � ���� ��������� ������������������ �����:
1) ������� ������� (���������� ���������)
2) ������������ ��������� ������� �������� ������ ��������� (��������� "*")
3) ����������� �������� � ������ ��������� �������
4) ������� ������
5) ����� ������� �� ����� (���������)
6) ��������� ��������
7) ������� � ���� 1.
*/

#include <stdio.h>
#include <time.h>
#include <windows.h>
#define n 20
#define m 36

void ClearArray (char (*Arr)[m])
{
	int i,j;
	for (i=0; i<n; i++)
		for (j=0; j<m; j++)
			Arr[i][j]= ' ';
}
void NewArray (char (*Arr)[m])
{
	int i, j;
	for (i=0; i<n/2; i++)
		for (j=0; j<m/2; j++)
			Arr[i][m-j-1]=Arr[i][j];
	for (i=0; i<n/2; i++)
		for (j=0; j<m; j++)
			Arr[n-i-1][j]=Arr[i][j];
}
void ClearScreen()
{
	system ("cls");
}
void PrintArray(char (*Arr)[m])
{
	int i, j;
	for (i=0; i<n; i++)
	{
		for (j=0; j<m; j++)
			printf ("%c", Arr[i][j]);
		printf ("\n");
	}
}
int kaleydoskop()
{
	int i,j;
	char Arr[n][m];
	while (1)
	{
	ClearArray(Arr); 
	srand(time(NULL));
	for (i=0; i<n/2; i++)
		for (j=0; j<m/2; j++)
		{
			if ((rand()%2)==0)
				Arr[i][j]='*';
		}
	NewArray(Arr);
	ClearScreen();
	PrintArray(Arr);
	Sleep(1000);
	}
	return 0;
}
int main ()
{
	do
	kaleydoskop();
	while (!kaleydoskop);
}