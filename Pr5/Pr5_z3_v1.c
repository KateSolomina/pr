/*
�������� ���������, �������������� ��������� ������� ������� ������� �����
������ ������ ���������� �����, ����� ������� � ����������, �� ���� ������
� ����� ����� �������� �� ������.
���������:
��������� ��������� ������������ ��������� ���� � ������ ��� ���������.
��� ������ ������ ����������� �������� �� ����� � ����������� ��������� 
������� �����.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define n 256
#define m 256

char SortWord(char *pArr)
{
	int i,k;
	int count, tmp_num;
	char temp;
	srand(time(NULL));
	
	count=strlen(*(&pArr))-1;
	for (k=1; k<count-1; k++)
	{
		tmp_num=rand()% (count-k-1);
		temp=pArr[k];
		pArr[k]=pArr[k+tmp_num];
		pArr[k+tmp_num]=temp;
	}
	
	printf ("%s ", pArr);
}

char getWords(char *Arr, char **pArr, int *j)
{
	int i,flag;
	int str_len;
	flag=0;

	str_len=strlen(*(&Arr));

	for (i=0; i<str_len; i++)
	{
		if (flag==0 && Arr[i]!=' ')
		{
			flag=1;
			pArr[*j]=&Arr[i];
			(*j)++;
		}
		else if (flag==1 && Arr[i]==' ')
		{ 
			flag=0;
			Arr[i]='\0';
		}
	}
	return pArr;
}
int main ()
{
	char Arr[n][m];
	char *pArr[n];
	int i=0, k, j;

	FILE *fp;

	fp = fopen("Pr5_z3.txt", "rt");
	if (fp == NULL)
	{
		puts ("File error!");
		return 1;
	}

	while (fgets (Arr[i],m, fp))
	{
		if (Arr[i][0]=='\n')
			break;
		else
		{
			j=i;
			printf ("%s", Arr[i]);
			getWords(Arr[i], pArr, &i);
			for (k=j; k<i; k++)
				SortWord(pArr[k]);
			printf ("\n");
		}
	}
	
	fclose (fp);

	return 0;
}