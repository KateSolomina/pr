/*
�������� ���������, ������� ��������� �� ������������ ������ � ������� �� �� �����,
��������� ����� � ��������� �������.
���������:
��������� ������ �������� ��� ������� �� ���� �������:
�) printWord - ������� ����� �� ������ (�� ����� ������ ��� �������)
�) getWords - ��������� ������ ���������� �������� ������ ���� ����
�) main - �������� �������
*/

#include <stdio.h>
#include <time.h>

void printWord(char *temp)
{
	while (*temp != '\0')
	{
		putchar (*temp);
		if(*(temp + 1) != '\0')
			temp++;
		else
			break;
	}
	putchar(' ');
}
char getWords(char *Arr, char **pArr, int *j)
{
	int i,flag;
	int str_len;
	flag=0;

	str_len=strlen(Arr);

	for (i=0; i<str_len; i++)
	{
		if (flag==0 && Arr[i]!=' ')
		{
			flag=1;
			pArr[*j]=&Arr[i];
			(*j)++;
		}
		else if (flag==1 && Arr[i]==' ')
		{ 
			flag=0;
			Arr[i]='\0';
		}
	}
	return pArr;
}
int main()
{
	char Arr[256];
	char *pArr[256];
	char *temp;
	int i,j=0, count;
	srand(time(NULL));
	printf ("Enter string\n");
	gets(Arr);
	
	getWords(Arr, pArr, &j);
	i=strlen(pArr);

	for (i=0; i<j; i++)
	{
		count=rand()%(j-i);
		printWord(pArr[count]);
		temp=pArr[count];
		pArr[count]=pArr[j-i-1];
		pArr[j-i-1]=temp;
	}

	printf ("\n");
	return 0;
}