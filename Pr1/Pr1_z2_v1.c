/*
�������� ���������, ������� ����������� ������� ����� � ������� ��:��:��,
� ����� ������� ����������� � ����������� �� ���������� ("������ ����", "������ ����" � �.�.)
*/
#include <stdio.h>
#include <math.h>

int main ()
{
	int cc, mm=0, ss=0;
	printf ("Enter time? (CC:MM:SS)\n");
	scanf ("%d:%d:%d", &cc, &mm, &ss);

	if (cc<0 || cc>24 || mm<0 || mm>60 || ss<0 || ss>60)
	{
		printf ("Error. Uncorrect time\n");
	}
	else
	{
		if (cc>=0 && cc<6)
			printf ("GoodNight\n");
		else if (cc>=6 && cc<12)
			printf ("GoodMorning\n");
		else if (cc>=12 && cc<18)
			printf ("GoodAfternoon\n");
		else if (cc=24 && (mm!=0 || ss!=0))
			printf ("Error\n");
		else 
			printf ("GoodEvening\n");
		printf ("Time: %d:%d:%d\n", cc, mm, ss);
		return 0;
	}	
}