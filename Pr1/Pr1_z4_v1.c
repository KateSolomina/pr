/*�������� ���������, ������� ��������� ���� �� ������������ ������� 
(����, �����) � ����������� (����������). ������ �������� � ���� ���� ����� �����,
��������� � ���� ������������� ����� � ��������� �� ������ �����. 1���=12������, 1����=2.54��
*/
#include <stdio.h>
#include <math.h>

int main ()
{
	unsigned int fut, dume;
	const double constFut=12.0, constSm=2.54;
	double sm;

	printf ("Enter fut:\n");
	scanf ("%d", &fut);
	printf ("Enter dume:\n");
	scanf ("%d", &dume);

	sm=(fut*constFut+dume*1.0)*constSm;
	printf ("%d fut and %d dume=%6.1lf sm\n",fut, dume, sm);
	return 0;
}