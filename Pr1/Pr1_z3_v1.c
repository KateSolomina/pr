/* �������� ���������, ������� ��������� �������� ���� �� �������� � ������� �, 
��������, � ����������� �� ������� ��� �����.
��������: 45.00D  - �������� �������� � ��������, 45.00R-� ��������. 
���� ������ �������������� �� ������� %f%c
*/
#include <stdio.h>
#include <math.h>

int main ()
{
	float dig;
	char ch;
	const float Pi=3.14159;
	int count=0;

	printf ("Enter angle (XX.XXD or XX.XXR)\n");
	scanf ("%f%c", &dig,&ch);

	if (ch=='D')
	{
		dig=dig*Pi/180.0f;
		printf ("angle=%f\n", dig);
	}
	else if (ch=='R')
	{
		dig=dig*180.0f/Pi;
		while (dig>360.0f) 
		{
			dig-=360;
			count++;
		}
		printf ("angle=%d*Pi %f\n", count, dig);
	}
	else
	{
		printf ("error\n");
	}
	return 0;
}