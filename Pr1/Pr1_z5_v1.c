/*
Написать программу, которая принимает строку от пользователя
и выводит ее на экран , выравнивая по центру
*/

#include <stdio.h>
#include <math.h>
#include <windows.h>

int main ()
{
	char ArrStr[80];
	const int x = 80,y = 25;	//working area of the screen
	int length,i, Str;

	printf ("Enter string\n");
	gets(ArrStr);
	length=strlen(ArrStr);
	//Str=length;					//the number of elements in the array
	length=(x+length)/2;		//middle of the workspace horizontally

	system ("cls");				//Clear Screen

	fprintf(stdout,"%*s\n",length,ArrStr);

	return 0;
}