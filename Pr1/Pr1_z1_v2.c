/*
�������� ���������, ������� ����������� � ������������ ���, ���� � ���, 
� ����� ����������� ����������� ����� � ����, ������� ������������
� ���������� ��������� (��������, ����������, �����)
������������ �������: ������=���/(����(�)*����(�))
��� ������ �����: 19<=������<=24
��� ������ �����: 18<=������<=25
*/
#include <stdio.h>
#include <math.h>

int main ()
{
	char Pol;
	double Rost;
	double Ves;
	double Index;

	const double w1=19.0, w2=24.0, m1=18.0, m2=25.0;

	printf ("Vvedite pol w/m \n");
	scanf ("%c",&Pol);
	if (Pol!='w' && Pol!='W' && Pol!='m' && Pol!='M')
		printf ("Error\n");
	else
	{
		printf ("Vvedite rost \n");
		scanf ("%lf",&Rost);
		if (Rost<0)
			printf ("Error\n");
		else
		{
			printf ("Vvedite ves \n");
			scanf ("%lf",&Ves);
			if (Ves<0)
				printf ("Error\n");
			else
			{
				Index=Ves/(Rost*Rost/10000.0);

				switch (Pol)
				{
				case 'w': 
				case 'W':
					{
						if (Index>=w1 && Index<=w2)
							printf ("Vash ves v norme \n");
						else if (Index<w1)
							printf ("Vam nado potolstet' \n");
						else
							printf ("Vam nado pohudet' \n");
						break;
					}
				case 'm': 
				case 'M':
					{
						if (Index>=m1 && Index<=m2)
							printf ("Vash ves v norme \n");
						else if (Index<w1)
							printf ("Vam nado potolstet' \n");
						else
							printf ("Vam nado pohudet' \n");
						break;
					}
				}
			}
		}
	}
	return 0;
}